public class SumOfIntervals {

    public static int sumIntervals(int[][] intervals) {
        boolean merged;

        do {
            merged = false;
            for (int i = 0; i < intervals.length; i++) {
                for (int j = 0; j < intervals.length; j++) {
                    if (intervals[i] != intervals[j]
                            && !(intervals[i][0] == 0 && intervals[i][1] == 0) && !(intervals[j][0] == 0 && intervals[j][1] == 0)
                            && (intervals[i][0] <= intervals[j][1] && intervals[i][0] >= intervals[j][0] || intervals[j][0] <= intervals[i][1] && intervals[j][0] >= intervals[i][0])) {

                        int[] merging = {0,0};
                        merging[0] = Math.min(intervals[i][0], intervals[j][0]);
                        merging[1] = Math.max(intervals[i][1], intervals[j][1]);

                        intervals[i] = merging;
                        intervals[j] = new int[]{0, 0};
                        merged = true;
                    }
                }
            }
        } while (merged);

        int total = 0;
        for (int[] i : intervals) {
            total += i[1] - i[0];
        }
        return total;
    }
}