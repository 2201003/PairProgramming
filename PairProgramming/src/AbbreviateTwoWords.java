public class AbbreviateTwoWords { // 8-kyu

    public static String abbrevName(String name) {
        String[] toAbbrev = name.split(" ");

        for (int i = 0; i < toAbbrev.length; i++) {
            if (i == 0)
                name = "" + toAbbrev[i].toUpperCase().charAt(0);
            else
                name = name.concat("." + toAbbrev[i].toUpperCase().charAt(0));
        }
        return name;
    }

}