public class Snail { // 4-kyu

    public static int[] snail(int[][] array) {
        if (array[0].length == 0) return new int[0];

        int x = 0, y = 0, l = array.length, index = 0;
        int xLimit1 = l, yLimit1 = l, xLimit2 = 1, yLimit2 = 0;
        byte cases = 0;

        int[] result = new int[l * l];
        while (index < result.length) {
            switch (cases) {
                case 0 -> {
                    if (y < yLimit1) {
                        result[index] = array[x][y];
                        y++;
                        index++;
                    } else {
                        y--;
                        x++;
                        yLimit1 -= 1;
                        cases = 1;
                    }
                }
                case 1 -> {
                    if (x < xLimit1) {
                        result[index] = array[x][y];
                        x++;
                        index++;
                    } else {
                        x--;
                        y--;
                        xLimit1 -= 1;
                        cases = 2;
                    }
                }
                case 2 -> {
                    if (y >= yLimit2) {
                        result[index] = array[x][y];
                        y--;
                        index++;
                    } else {
                        y++;
                        x--;
                        yLimit2 += 1;
                        cases = 3;
                    }
                }
                case 3 -> {
                    if (x >= xLimit2) {
                        result[index] = array[x][y];
                        x--;
                        index++;
                    } else {
                        x++;
                        y++;
                        xLimit2 += 1;
                        cases = 0;
                    }
                }
            }
        }
        return result;
    }
}
