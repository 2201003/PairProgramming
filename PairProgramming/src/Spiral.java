public class Spiral {

    public static int[][] spiralize(int size) {
        if(size <= 0) return null;
        int[][] spiral = new int[size][size];
        int minCol = 0;
        int maxCol = size-1;
        int minRow = 0;
        int maxRow = size-1;

        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral.length; j++) spiral[i][j] = 0;
        }

        while (minRow <= maxRow){
            for (int i = minCol; i <= maxCol; i++)
                spiral[minRow][i] = 1;
            for (int i = minRow; i <= maxRow; i++)
                spiral[i][maxCol] = 1;

            if(minCol != 0)
                minCol+=1;
            if(maxRow-1 == minRow)
                break;

            for (int i = maxCol-1; i >= minCol; i--)
                spiral[maxRow][i] = 1;
            for (int i = maxRow-1; i >= minRow+2; i--)
                spiral[i][minCol] = 1;

            minCol+=1;
            minRow+=2;
            maxCol-=2;
            maxRow-=2;
        }
        return spiral;
    }
}
