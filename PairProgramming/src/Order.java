public class Order { // 6-kyu

    public static String order(String words) {
        String sorted = "";
        String[] wordsToSort = words.split(" ");

        for (int x = 0; x < wordsToSort.length; x++) {
            for (int y = 0; y < wordsToSort.length; y++) {
                if (wordsToSort[y].contains("" + (x + 1))) {
                    if (x == 0) {
                        sorted = sorted.concat(wordsToSort[y]);
                    } else {
                        sorted = sorted.concat(" " + wordsToSort[y]);
                    }
                    break;
                }
            }
        }
        return sorted;
    }

}
